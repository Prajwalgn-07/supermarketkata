package ProductCheckoutTest;

import SuperMarketCounter.Invoice;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OfferDetailsTest {
    List<String> items;
    Invoice invoice;
    @BeforeMethod
    public void setUp() throws IOException {
        invoice=new Invoice();
    }
    @Test
    public void offerTest(){
        items=new ArrayList<>();
        items.add("A");
        items.add("A");
        items.add("A");
        items.add("B");
        items.add("B");
        Assert.assertEquals(invoice.calculate(items),175);
    }

}
