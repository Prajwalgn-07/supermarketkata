package ProductCheckoutTest;


import SuperMarketCounter.Invoice;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ItemDetailsTest {
   List<String>items;
   Invoice invoice;
   @BeforeMethod
   public void setUp() throws IOException {
      invoice=new Invoice();
   }

   @Test
   public void itemATest(){
      items=new ArrayList<>();
      items.add("A");
      Assert.assertEquals(invoice.calculate(items),50);
   }
   @Test
   public void itemBTest(){
      items=new ArrayList<>();
      items.add("B");
      Assert.assertEquals(invoice.calculate(items),30);
   }
   @Test
   public void itemCTest(){
      items=new ArrayList<>();
      items.add("C");
      Assert.assertEquals(invoice.calculate(items),20);
   }
   @Test
   public void itemDTest(){
      items=new ArrayList<>();
      items.add("D");
      Assert.assertEquals(invoice.calculate(items),15);
   }

}
