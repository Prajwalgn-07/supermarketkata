package ProductCheckoutTest;

import SuperMarketCounter.Invoice;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InvoiceTest {
    List<String> items;
    Invoice invoice;
    @BeforeMethod
    public void setUp() throws IOException {
        invoice=new Invoice();
    }
    @Test
    public void itemATest(){
        items=new ArrayList<>();
        items.add("A");
        items.add("A");
        items.add("A");
        items.add("A");
        items.add("B");
        items.add("B");
        items.add("B");
        items.add("B");
        items.add("B");
        items.add("C");
        items.add("D");
        items.add("D");
        Assert.assertEquals(invoice.calculate(items),350);
    }
}
