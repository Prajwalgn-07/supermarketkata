package SuperMarketCounter;

import SuperMarketDetails.ItemDetails;
import SuperMarketDetails.OfferDetails;
import Utils.PropertyReader;

import java.io.IOException;
import java.util.List;

public class Invoice {
    private double priceOfItemA;
    private double priceOfItemB;
    private double priceOfItemC;
    private double priceOfItemD;
    private double offerPriceOfA;
    private double offerCountOfA;
    private double offerPriceOfB;
    private double offerCountOfB;
    private double itemACount=0;
    private double itemBCount=0;
    private double itemCCount=0;
    private double itemDCount=0;
    private double totalAmount=0;
    PropertyReader propertyReader;


    public Invoice() throws IOException {
        propertyReader=new PropertyReader("./src/main/resources/PriceTag.properties");
        initializePrice();
    }

    public double calculate(List<String>Items){
        setCount(Items);
        OfferDetails offerDetails=new OfferDetails(offerCountOfA,offerPriceOfA,offerCountOfB,offerPriceOfB);
        totalAmount= offerDetails.offerOnA(itemACount,priceOfItemA)+offerDetails.offerOnB(itemBCount,priceOfItemB)
                +new ItemDetails(priceOfItemC,itemCCount).itemPrice()+new ItemDetails(priceOfItemD,itemDCount).itemPrice();
        return totalAmount;
    }

    public void initializePrice(){
        priceOfItemA=stringToDouble(propertyReader.getProperty("priceOfItemA"));
        priceOfItemB=stringToDouble(propertyReader.getProperty("priceOfItemB"));
        priceOfItemC=stringToDouble(propertyReader.getProperty("priceOfItemC"));
        priceOfItemD=stringToDouble(propertyReader.getProperty("priceOfItemD"));
        offerCountOfA=stringToDouble(propertyReader.getProperty("offerCountOfA"));
        offerPriceOfB=stringToDouble(propertyReader.getProperty("offerPriceOfB"));
        offerPriceOfA=stringToDouble(propertyReader.getProperty("offerPriceOfA"));
        offerCountOfB=stringToDouble(propertyReader.getProperty("offerCountOfB"));
    }

    public void setCount(List<String>Items){
        for(String item:Items){
            if(item.equals("A"))
                itemACount+=1;
            if(item.equals("B"))
                itemBCount+=1;
            if(item.equals("C"))
                itemCCount+=1;
            if(item.equals("D"))
                itemDCount+=1;
        }
    }

    public double stringToDouble(String value){
        return Double.parseDouble(value);
    }

}
