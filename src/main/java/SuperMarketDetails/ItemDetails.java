package SuperMarketDetails;

public class ItemDetails implements PriceDetails {
    private  double priceOfItem;
    private double itemCount;

    public ItemDetails( double priceOfItem, double itemCount) {
        this.priceOfItem=priceOfItem;
        this.itemCount=itemCount;
    }

    @Override
    public double itemPrice(){
        return this.priceOfItem*this.itemCount;
    }
}
