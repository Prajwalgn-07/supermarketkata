package SuperMarketDetails;

public class OfferDetails {
    private double offerPriceOfA;
    private double offerCountOfA;
    private double offerPriceOfB;
    private double offerCountOfB;

    public OfferDetails(double offerCountOfA,double offerPriceOfA,double offerCountOfB,double offerPriceOfB){
        this.offerPriceOfA=offerPriceOfA;
        this.offerCountOfB=offerCountOfB;
        this.offerCountOfA=offerCountOfA;
        this.offerPriceOfB=offerPriceOfB;
    }

    public double offerOnA(double itemACount, double priceOfItemA){
        double remainder=itemACount%offerCountOfA;
        double tempPrice=priceOfItemA;
        itemACount-=remainder;
        tempPrice=remainder*priceOfItemA;
        itemACount = itemACount / offerCountOfA;
        priceOfItemA = (offerCountOfA* priceOfItemA) - offerPriceOfA;
        priceOfItemA=priceOfItemA*itemACount;
        return priceOfItemA+tempPrice;
    }
    public double offerOnB(double itemBCount, double priceOfItemB){
        double remainder=itemBCount%offerCountOfB;
        double tempPrice=priceOfItemB;
        itemBCount-=remainder;
        tempPrice=remainder*priceOfItemB;
        itemBCount = itemBCount / offerCountOfB;
        priceOfItemB = (offerCountOfB* priceOfItemB) - offerPriceOfB;
        priceOfItemB=priceOfItemB*itemBCount;
        return priceOfItemB+tempPrice;
    }
}
